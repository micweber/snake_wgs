import pandas as pd
import os
from Bio import SeqIO


##from folder.A import *

configfile: "config.yaml"

## Read the config samples
samples = pd.read_table(config["samples"]).set_index("ID",drop=False)

## Check which sample IDs have a Null value
samples_fastq = samples[~samples["File_left_reads"].isnull().values]
samples_fasta = samples[~samples["File_assembly"].isnull().values]

## Link the samples_fasta files
for row in samples_fasta.itertuples():
  os.makedirs("results/finalAssembly", exist_ok=True)
  os.makedirs("results_filtered/finalAssembly", exist_ok=True)
  src = row.File_assembly
  dst = "results/finalAssembly/" + row.ID + ".fasta"
  dst2 = "results_filtered/finalAssembly/" + row.ID + ".fasta"
  if not os.path.exists(dst):
    os.symlink(src, dst)
  if not os.path.exists(dst2):
    os.symlink(src, dst2)


## Define snakemake rules
include: "rules/link_input.smk"
include: "rules/bakta.smk"
include: "rules/fastqc.smk"
include: "rules/kraken2.smk"
include: "rules/shovill.smk"
#include: "rules/coverage_V2.smk"
include: "rules/plasmidspades.smk"
include: "rules/quast.smk"
include: "rules/prokka.smk"
include: "rules/platon.smk"
include: "rules/mlst.smk"
#include: "rules/multiqc.smk"
include: "rules/abricate.smk"
include: "rules/amrfinderplus.smk"
include: "rules/spoligotyping.smk"
include: "rules/trimquality.smk"
include: "rules/ksnp.smk"
#include: "rules/snippy2.smk"
include: "rules/panaroo.smk"
include: "rules/metaphlan.smk"
#include: snakefiles + "snippy.smk"


## Rule order
#ruleorder: renameContigs > link_samples_fasta
#ruleorder: filter_kraken > shovill_filtered
ruleorder: kraken2 > filter_kraken


SHOVILL_RESULTS =  expand("results/shovill/{sample}",sample=samples_fastq.ID)
SHOVILL_FINAL_ASSEMBLIES = expand("results/assemblies/{sample}.fna",sample=samples.ID)

QUAST_RESULT = "results/quast/allquast.csv"

KRAKEN_DIR_RESULTS = expand("results/kraken2/{sample}",sample=samples_fastq.ID)
KRAKEN_RESULTS = expand("results/kraken2/{sample}/report.txt",sample=samples_fastq.ID)

#KRONA_FILE	  expand("results/kraken2/{sample}/{sample}.krona", sample=samples_fastq.ID)
#KRONA_HTML = expand("results/kraken2/{sample}/{sample}_krona.html", sample=samples_fastq.ID)

METAPHLAN_RESULTS = expand("results/metaphlan/profiles/{sample}_profiled.txt",sample=samples_fastq.ID)

BAKTA_RESULTS = expand("results/bakta/{sample}/{sample}.gff3",sample=samples.ID)
PROKKA_RESULTS = expand("results/prokka/{sample}",sample=samples.ID)

SPOLIGOTYPING = "results/spoligotyping/all_spoligotyping.csv"

PLATON_RESULTS = "results/platon/platon_summary.tsv"
PLASMID_SPADES = expand("results_filtered/spades_plasmid/{sample}", sample=samples_fasta.ID),

## SNP Typing
SNIPPY_DIRS = expand("results/snippy/{sample}", sample=samples_fastq.ID),
SNIPPY_CORE = "results/snippy_core/core.aln"
SNIPPY_CLEAN = "results/snippy_core/clean.full.aln"
SNIPPY_GUBBINS = "results/snippy_core/gubbins.filtered_polymorphic_sites.fasta"
SNIPPY_TREE = "results/trees/snippy_fasttree.nw"

KSNP_TREE = "results/trees/kSNP_NJ_tree.pdf"
KSNP_TREE = "results/trees/kSNP_ML_tree.pdf"
KSNP_TREE = "results/trees/kSNP_parsimoney_tree.pdf"

## MULTIQC
FASTQC_DIR = "results/multiqc_fastqc"
MULTIQC_DIR = "results/multiqc"
MQ_COV_PAGE = "results/coverage/Coverage_fastq_info_mqc.txt"
MQ_MLST_PAGE = "results/mlst/Classical_MLST_mqc.txt"



rule all:
  input:
    expand("input/{sample}_R1.fastq.gz",sample=samples_fastq.ID),
    expand("input/{sample}_R2.fastq.gz",sample=samples_fastq.ID),
    "results/mlst/Classical_MLST_mqc.txt",
    BAKTA_RESULTS,
    expand("results/kraken2/{sample}/{sample}.report.txt",sample=samples_fastq.ID),   
   # "results_filtered/snippy_core/core.aln", ## Snippy core results
    expand("results/metaphlan/profiles/{sample}_profiled.txt",sample=samples_fastq.ID),
    SPOLIGOTYPING,
PLASMID_SPADES = expand("results_filtered/spades_plasmid/{sample}", sample=samples_fasta.ID),
#   expand("results_filtered/snippy/{sample}",sample=samples_fastq.ID) # snippy results
#    expand("results/kraken2/{sample}/report.txt",sample=samples_fastq.ID)
#	  expand("results/kraken2/{sample.ID}", sample=samples_fastq.itertuples()),
#	  expand("results/kraken2/{sample.ID}/{sample.ID}.krona", sample=samples_fastq.itertuples()),
#	  expand("results/kraken2/{sample.ID}/{sample.ID}_krona.html", sample=samples_fastq.itertuples()),
#   unfiltered
#    expand("results/shovill/{sample.ID}",sample=samples_fastq.itertuples()),
#    expand("results/finalAssembly/{sample}.fasta",sample=samples_fasta.ID),
#    "results/quast/allquast.csv",
#	  "results/quast/quast",
#    expand("results/prokka/{sample}",sample=samples.ID),
#    "results/platon/platon_summary.tsv",
#	  "results/coverage/Coverage_fastq_info_mqc.txt",
#	  "results/multiqc_fastqc",
#"results/multiqc"
#	  "results/trees/kSNP_NJ_tree.pdf",
#	  "results/trees/kSNP_ML_tree.pdf",
#	  "results/trees/kSNP_parsimoney_tree.pdf",
#   filtered
#    expand("results_filtered/snippy/{sample.ID}", sample=samples_fastq.itertuples()),
#    "results_filtered/snippy_core/core.aln",
#    "results_filtered/snippy_core/clean.full.aln",
#    "results_filtered/snippy_core/gubbins.filtered_polymorphic_sites.fasta",
#    "results_filtered/trees/snippy_fasttree.nw",
#	  expand("results_filtered/kraken2/{sample.ID}", sample=samples_fastq.itertuples()),
#    expand("results_filtered/shovill/{sample.ID}",sample=samples_fastq.itertuples()),
#    expand("results_filtered/finalAssembly/{sample.ID}.fasta",sample=samples_fasta.itertuples()),
#    expand("results_filtered/spades_plasmid/{sample.ID}", sample=samples_fasta.itertuples()),
#    "results_filtered/quast/allquast.csv",
#	  "results_filtered/quast/quast",
#    "results_filtered/coverage/Coverage_fastq_info_mqc.txt",
#  "results_filtered/multiqc"
