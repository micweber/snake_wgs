
"""
Prokka for gene prediction and annotation

"""
rule prokka:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        directory("results/prokka/{sample}")
    threads:
        8
    benchmark:
        "log/benchmark/prokka/{sample}.txt"
    log:
        "log/prokka/{sample}.txt"
    conda:
        "../envs/prokka.yaml"
    params:
        prefix="{sample}",
        gcode=config["genetic_code"],
        proteins=config.get("annotations",""),
        outdir="results/prokka/{sample}"
    shell:
        """
        prokka --cpus {threads}  --force --kingdom Bacteria --gcode {params.gcode} {params.proteins}\
            --outdir {params.outdir}\
            --prefix {params.prefix} \
            {input} &> {log}
        """
        
        
# rule prokka_filtered:
#     input:
#         "results_filtered/finalAssembly/{sample}.fasta"
#     output:
#         directory("results_filtered/prokka/{sample}")
#     threads:
#         8
#     benchmark:
#         "log/benchmark/prokka/filtered/{sample}.txt"
#     log:
#         "log/prokka/filtered/{sample}.txt"
#     conda:
#         "../envs/prokka.yaml"
#     params:
#         prefix="{sample}",
#         gcode=config["genetic_code"],
#         proteins=config["annotations"],
#         outdir="results_filtered/prokka/{sample}"
#     shell:
#         """
#         prokka --cpus {threads}  --force --kingdom Bacteria --gcode {params.gcode} --proteins {params.proteins}\
#             --outdir {params.outdir}\
#             --prefix {params.prefix} \
#             {input} &> {log}
#         """