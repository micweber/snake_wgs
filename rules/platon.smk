

"""
Determine plasmid sequences using blast and PLSDB

"""

# Input are the filtered assemblies from spades assembly

rule platon:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        touch("results/platon/{sample}/{sample}.tsv")
    log:
        "log/platon/{sample}.log"
    benchmark:
        "benchmark/platon/{sample}_platon.txt"
    conda:
        "../envs/platon.yaml"
    params:
      platondb=config.get("platon_db",""),
      outdir="results/platon/{sample}"
    threads: 4
    shell:
        """
        mkdir -p {params.outdir}
        platon --db {params.platondb} --threads {threads} --output {params.outdir} {input} > output
        """

rule platon_summary:
	input:
		expand("results/platon/{sample}/{sample}.tsv", sample=samples.ID)
	output:
		tsv=touch("results/platon/platon_summary.tsv"),
		xlsx=touch("results/platon/platon_summary.xlsx")
	log:
		"log/platon/platon_summary.log"
	conda:
	  "../envs/rcran.yaml"
	script:
		"../scripts/combine_platon.R"
		
