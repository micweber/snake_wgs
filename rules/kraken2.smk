"""
Contamination Control
taxonomic investigation of reads
"""


#receive all forward and reverse fastq files (in a gzippes format)
#create output directories before running

rule kraken2:
    input:
        fw="results/input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz",
        rv="results/input_trimmed/{fastq}/{fastq}_R2_val_2.fq.gz"
    output:
        directory("results/kraken2/{fastq}"),
        kraken=temp("results/kraken2/{fastq}/{fastq}.kraken"),
        report="results/kraken2/{fastq}/{fastq}.report.txt"
    threads: 8
    params:
        db=config.get("kraken2db",{}),
        outdir="results/kraken2/{fastq}"
    benchmark:
        "log/benchmark/kraken2/{fastq}.txt"
    log:
        "log/kraken2/{fastq}.log"
    conda:
        "../envs/kraken2.yaml"
    resources: mem_mb=60000, cpus=8    
    priority: 2
    shell:
        """
        mkdir -p {params.outdir} &&
        kraken2 --use-names --threads {threads} --db {params.db} --report {output.report} --gzip-compressed --paired {input.fw} {input.rv} > {output.kraken} 2> {log}
        """



rule kraken2krona:
    input:
        report= rules.kraken2.output.report
    output:
        krona= "results/kraken2/{fastq}/{fastq}.krona",
        html= "results/kraken2/{fastq}/{fastq}_krona.html"
    benchmark:
        "log/benchmark/kraken2/{fastq}_krona.txt"
    log:
        "log/kraken2/{fastq}_krona.log"
    conda:
         "../envs/krona.yaml"
    priority: 2
    shell:
        "python scripts/KrakenTools-master/kreport2krona.py -r {input.report} -o {output.krona} && ktImportText {output.krona} -o {output.html} 2> {log}"

rule filter_kraken:
    input:
        fw="results/input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz",
        rv="results/input_trimmed/{fastq}/{fastq}_R2_val_2.fq.gz",
        kraken= rules.kraken2.output.kraken,
        report= rules.kraken2.output.report
        #kraken= "results/kraken2/{fastq}/{fastq}.kraken",
        #report= "results/kraken2/{fastq}/report.txt"
    output:
        directory("results_filtered/kraken2/{fastq}"),
        o1=temp("results_filtered/kraken2/{fastq}/{fastq}_R1_krakenFiltered.fastq.gz"),
        o2=temp("results_filtered/kraken2/{fastq}/{fastq}_R2_krakenFiltered.fastq.gz")
    params:
        taxid=config.get("taxid",0),
        outdir="results_filtered/kraken2/{fastq}",
        exclude=config.get("exclude",0)    
    benchmark:
        "log/benchmark/kraken2/filtered/{fastq}.txt"
    log:
        "log/kraken2/filtered/{fastq}.log"
    priority: 2
    run:
      shell("mkdir -p {params.outdir}")
      if {params.exclude}:
        shell("python scripts/KrakenTools-master/extract_kraken_reads.py -k {input.kraken} -s1 {input.fw} -s2 {input.rv} -r {input.report} -o {params.outdir}/{wildcards.fastq}_R1_krakenFiltered.fastq -o2 {params.outdir}/{wildcards.fastq}_R2_krakenFiltered.fastq --fastq-output --taxid {params.taxid} --exclude --include-children")
        shell("gzip {params.outdir}/*.fastq && echo excluded sequences from Organisms with NCBI TaxId {params.taxid} > results_filtered/filter_params.txt 2> {log}")
      else:
        shell("python scripts/KrakenTools-master/extract_kraken_reads.py -k {input.kraken} -s1 {input.fw} -s2 {input.rv} -r {input.report} -o {params.outdir}/{wildcards.fastq}_R1_krakenFiltered.fastq -o2 {params.outdir}/{wildcards.fastq}_R2_krakenFiltered.fastq --fastq-output --taxid {params.taxid} --include-children")
        shell("gzip {params.outdir}/*.fastq && echo included sequences from Organisms with NCBI TaxId {params.taxid} > results_filtered/filter_params.txt 2> {log}" )



## Process the Kraken report to summarise the most frequenct species and domains
rule combine_kraken_report:
    input:
        reports=expand("results/kraken2/{fastq}/{fastq}.report.txt",fastq=samples_fastq.ID),
        metadata=config['metadata']
    output:
        csv="results/kraken2/all_kraken_reports.csv",
        xlsx="results/kraken2/all_kraken_reports.xlsx"
    log:
        "log/kraken2/kraken_report.log"
    benchmark:
        "benchmark/kraken2/kraken_report.txt"
    conda:
        "../envs/rcran.yaml"
    priority: 2    
    script:
        "../scripts/read_kraken_report.R"
