
"""
Quast for statistics about all final assemblies
"""
# input is assembly files after filtering with coverage and length AND Reference genoem file
# collect_quast creates directory for each sample inlcuding tsv html ,...

rule quast:
    input:
        fasta="results/finalAssembly/{sample}.fasta",
    output:
        directory("results/quast/{sample}")
    params:
        ref=config["refstrain"]
    benchmark:
        "log/benchmark/quast/{sample}.txt"
    log:
        "log/quast/{sample}.txt"
    conda:
      "../envs/quast.yaml"
    shell:
       "quast.py -o {output} -R {params.ref} {input.fasta}  &>  {log}"

rule quast_filtered:
    input:
        fasta="results_filtered/finalAssembly/{sample}.fasta",
    output:
        directory("results_filtered/quast/{sample}")
    params:
        ref=config["refstrain"]
    benchmark:
        "log/benchmark/quast/filtered/{sample}.txt"
    log:
        "log/quast/filtered/{sample}.txt"
    conda:
      "../envs/quast.yaml"
    shell:
       "quast.py -o {output} -R {params.ref} {input.fasta}  &>  {log}"


"""
Quast for statistics about all final assemblies
"""

#input is assembly files after filtering with coverage and length AND Reference genoem file
#collect_quast creates directory for each sample inlcuding tsv html ,...

rule quast_single:
    input:
        fasta=expand("results/finalAssembly/{sample}.fasta", sample=samples.ID)
    output:
        outdir1=directory("results/quast/quast"),
        outdir2=directory("results/quast/quast_norf")
    params:
        ref=config["refstrain"]
    benchmark:
        "log/benchmark/quast/quast_single.txt"
    log:
        "log/quast/quast_log.txt"
    conda:
      "../envs/quast.yaml"
    shell:
    	"""
	      quast.py -o {output.outdir1} -R {params.ref} {input.fasta} &> {log}
	      quast.py -o {output.outdir2} {input.fasta} &> {log}
    	"""

rule quast_single_filtered:
    input:
        fasta=expand("results_filtered/finalAssembly/{sample}.fasta", sample=samples.ID)
    output:
        outdir1=directory("results_filtered/quast/quast"),
        outdir2=directory("results_filtered/quast/quast_norf")
    params:
        ref=config["refstrain"]
    benchmark:
        "log/benchmark/quast/filtered/quast_single.txt"
    log:
        "log/quast/filtered/quast_log.txt"
    conda:
      "../envs/quast.yaml"
    shell:
    	"""
	      quast.py -o {output.outdir1} -R {params.ref} {input.fasta} &> {log}
	      quast.py -o {output.outdir2} {input.fasta} &> {log}
    	"""


"""
Collects Quality of Assemblies
"""
# input is quast results for each sample
# output is csv and xls
rule collect_quast:
    input:
        localres=expand("results/quast/{sample}", sample=samples.ID),
    output:
        allcsv="results/quast/allquast.csv",
        allxls="results/quast/allquast.xlsx",
    benchmark:
        "log/benchmark/quast/allquast.txt",
    log:
        "log/quast/allquast.txt",
    conda:
        "../envs/rcran.yaml"
    script:
        "../scripts/combinequast.R"


rule collect_quast_filtered:
    input:
        localres=expand("results_filtered/quast/{sample}", sample=samples.ID),
    output:
        allcsv="results_filtered/quast/allquast.csv",
        allxls="results_filtered/quast/allquast.xlsx",
    benchmark:
        "log/benchmark/quast/filtered/allquast.txt",
    log:
        "log/quast/filtered/allquast.txt",
    conda:
        "../envs/rcran.yaml"
    script:
        "../scripts/combinequast.R"