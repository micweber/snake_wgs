"""2019.03.29 MYA"""


"""
snippy
Runs snippy for each fastq file
"""
rule snippy:
    input:#receive all forward and reverse fastq files
        fw= rules.trim_galore.output.fw,
        rv= rules.trim_galore.output.rv,
        ref=config["refstrain"]#reference
    output:
        snippy= directory("results_filtered/snippy/{fastq}")
    threads:
        16
    benchmark:
        "log/benchmark/snippy/{fastq}.txt"
    log:
        "log/snippy/filtered/{fastq}.log"
    conda:
      "../envs/snippy.yaml"
    priority: 4  
    shell:
        "snippy --force  --cpus {threads} --outdir {output.snippy} --ref {input.ref} --R1 {input.fw} --R2 {input.rv} 2>{log}"

"""
snippy_core
Combines snippy results into core genome alignment
"""

rule snippy_core:
    input:
        expand("results_filtered/snippy/{fastq}",fastq=samples_fastq.ID)
    output:
        aln= "results_filtered/snippy_core/core.aln",
        full= "results_filtered/snippy_core/core.full.aln",
        ref= "results_filtered/snippy_core/core.ref.fa",
        tab= "results_filtered/snippy_core/core.tab",
        txt= "results_filtered/snippy_core/core.txt",
        vcf= "results_filtered/snippy_core/core.vcf",
    params:
        ref=config["refstrain"]
    benchmark:
        "log/benchmark/snippy/snippy_core.txt"
    log:
       "log/snippy/filtered/snippy_core.log"
    conda: "../envs/snippy.yaml"
    priority: 4
    shell:
        "snippy-core  --ref {params.ref} --prefix results_filtered/snippy_core/core {input}  2>{log}"
        
        
"""
snippy_cleanup
clean-up the alignment (necessary to pass .aln to gubbins)
"""

rule snippy_cleanup:
  input:
      rules.snippy_core.output.full
  output:
      "results_filtered/snippy_core/clean.full.aln"
  conda: "../envs/snippy.yaml"
  benchmark:
      "log/benchmark/snippy/snippy_core_cleanup.txt"
  log:
      "log/snippy/filtered/snippy_core_cleanup.log"
  shell: "snippy-clean_full_aln {input} > {output}"


"""
Gubbins: removes sites with elevated densities of polymorphisms (recombination events)
suited for samples with limited diversity, sharing a recent common ancestor (strain, lineage)
not suited for looking at recombination across species-wide diversity
"""       
  

rule gubbins:
  input:
      rules.snippy_cleanup.output
  output:
      "results_filtered/snippy_core/gubbins.filtered_polymorphic_sites.fasta"
  conda: "../envs/gubbins.yaml"
  benchmark:
      "log/benchmark/snippy/snippy_core_gubbins.txt"
  log:
      "log/snippy/filtered/snippy_core_gubbins.log"
  params:
      date = config.get("isolation_dates",{}) #there should be a --date option that builds a time calibrated tree, but its not working  -> Todo: update to version 3.3
  shell: 
          "mkdir -p results_filtered/trees && \
          run_gubbins.py -p results_filtered/snippy_core/gubbins -t raxml {input} && \
          mv gubbins.*.tre ./results_filtered/trees/"


"""
Extraxction of SNPs from a multi-FASTA alignment
"""   
rule snp_sites:
  input:
      "results_filtered/snippy_core/gubbins.filtered_polymorphic_sites.fasta"
  output:
      "results_filtered/snippy_core/gubbins_cleaned.core.aln"
  conda: "../envs/snippy.yaml"
  benchmark:
      "log/benchmark/snippy/snippy_core_clean_snpsites.txt"
  log:
      "log/snippy/filtered/snippy_core_clean_snpsites.log"
  shell: "snp-sites -c {input} > {output}"
       
"""
fasttree
Runs fasttree on mutliple sequence alignment from snippy + gubbins
Returns tree with IDs in nodes as newick
"""

rule fasttree:
    input:
        rules.snp_sites.output
    output:
        tree="results_filtered/trees/snippy_fasttree.nw"    
    benchmark:
       "log/benchmark/snippy/snippy_fasttree.txt"
    log:
        "log/snippy/filtered/snippy_fasttree.log"
    conda: "../envs/fasttree.yaml"
    shell:
        "FastTree -gtr -nt {input} > {output} 2>{log}"

"""
snpdist
Convert a core alignment from snippy to SNP distance matrix
"""

rule snpdist:
    input:
        "results_filtered/snippy_core/core.aln",
    output:#all output from snippy-core
        snpdist= "results_filtered/snippy_core/snpdist.csv",    
    benchmark:
        "log/benchmark/snippy/snpdist.txt"
    log:
        "log/snippy/filtered/snpdist.log"
    conda: "../envs/snpdist.yaml"
    shell:
        "snp-dists {input.aln} > {output.snpdist} 2>{log}"


"""
computes tree from mutliple sequence alignment from snippy + gubbins
"""

rule compute_Tree_raxml:
    input:
        aln = rules.snp_sites.output
    output:
        tree = "results_filtered/trees/core.raxml",
    message: "Computing a tree from core alignment with raxml"
    conda:
        "../envs/raxml.yaml"
    #threads:
   #    config["parameters"]["threads"]
    log:
       "log/snippy/filtered/snippy_raxml.log"
    params:
       repeats = 20,
       model = "GTRGAMMA",
 #      dir = #"/home/results_filtered/trees"
    shell:
        "raxmlHPC -m {params.model} -p 12345 -s {input.aln} -# {params.repeats} -n {output.tree}" #-w {params.dir}"


"""
Draws tree from snippy results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed samples
"""

rule drawSnippytree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #Always us NEwick as variable name
        newick=rules.fasttree.output.tree,#tree from fasttree in newick fromat
    output:
        #Always use pdf as variable name
        pdf="results/trees/snippy_fasttree.pdf",
        newick="results/trees/snippy_fasttree_common.nw"#tree in newick format with exchanded IDs into common names
    params:
        #alsways use params as variable name
        title="Phylogenetic tree from Snippy and Fasttree"
    conda:
        "../envs/rcran.yaml"
    log:
        "log/snippy/filtered/drawSnippytree.log"
    benchmark:
        "log/benchmark/snippy/drawSnippytree.txt"
    script:
        "../scripts/plottree.R"
