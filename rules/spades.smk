"""
Assembly of the trimmed reads using spades.
"""
#receive all forward and reverse fastq files
#output: directory for each samples
#spades does quality control and trimming itself

rule spades:
    input:
        fw="input/{fastq}_R1.fastq.gz",
        rv="input/{fastq}_R2.fastq.gz"
    output:
        directory("results/spades/{fastq}")
    threads:
        42
    benchmark:
        "log/benchmark/spades/{fastq}.log"
    log:
        "log/shovill/{fastq}.log"
    conda:
      "../envs/spades.yaml"
    shell:
        "spades.py -t {threads} \
            --pe1-1 {input.fw} \
            --pe1-2 {input.rv} \
            --careful \
            -o {output}"




"""
Renames contigs to have short contig names (required by some tools)
"""

#input is result from spades
#input is result from spades

rule spades_rename_contigs:
    input:
        "results/spades/{fastq}/contigs.fasta"
    output:
        "results/finalAssembly/{fastq}.fasta"
    log:
        "log/spadesrename/{fastq}.log"
    conda:
        "../envs/bioawk.yaml"
    benchmark:
        "log/benchmark/spadesrename/{fastq}.log"
    shell:
        "scripts/fa_rename.sh {input} {output} &>  {log}"


"""
Assembly of plasmids using spades
"""
# receive all forward and reverse fastq files
# output is directoy for each sample
rule spades_plasmid:
    input:
        fw="input/{fastq}_R1.fastq.gz",
        rv="input/{fastq}_R2.fastq.gz"
    output:
        directory("results/spades_plasmid/{fastq}")
    threads:
        16
    benchmark:
        "benchmark/plasmidspades/{fastq}.txt"
    log:
        "log/plasmidspades/{fastq}.txt"
    conda:
      "../envs/spades.yaml"
    shell:
        """
        plasmidspades.py -t {threads} \
            --pe1-1 {input.fw} \
            --pe1-2 {input.rv} \
            --careful \
            -o {output}
            
        """    
