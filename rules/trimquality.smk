
rule trim_galore:
  input:
    fw="input/{fastq}_R1.fastq.gz",
    rv="input/{fastq}_R2.fastq.gz"
  output:
    fw=temp("results/input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz"),
    rv=temp("results/input_trimmed/{fastq}/{fastq}_R2_val_2.fq.gz")
  threads: 4
  params:
    outdir="results/input_trimmed/{fastq}"
  conda:
    "../envs/fastqc.yaml"
  resources:
    disk_mb=100000
  priority: 1
  shell:
    """
		trim_galore --paired --phred33 --cores {threads} --fastqc -q 20 --length 100 -o {params.outdir} {input.fw} {input.rv}
    """

# rule trim_processed:
#   input:
#     fw="results/input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz",
#     rv="results/input_trimmed/{fastq}/{fastq}_R2_val_2.fq.gz"
#   output:
#     fw="results/input_processed/{fastq}/{fastq}_R1.fastq.gz"
#     rv="results/input_processed/{fastq}/{fastq}_R2.fastq.gz"
#   run:
#     os.symlink(output.fw,input.fw)
#     os.symlink(output.rv,input.rv)