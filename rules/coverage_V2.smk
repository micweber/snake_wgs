


"""
Calculate coverage for each sample
"""

#adapted from https://github.com/raymondkiu/fastq-info/blob/master/fastq_info_3.sh
#input are zipped fastq-files AND reference genome assembly

rule coverage:
    input:
      fw="input/{fastq}_R1.fastq.gz",
      rv="input/{fastq}_R2.fastq.gz",
      ref=config["refstrain"]
    output:
      out="results/coverage/{fastq}.csv"
    log:
       "log/coverage/{fastq}.log"
    shell:
       "sh scripts/fastq_info_ma_V2.sh  {input.fw} {input.rv} {input.ref} > {output.out} 2>{log}"

"""
Collects coverage result from ech sample and combines in one file
"""
rule collect_coverage:
    input:
        localres=expand("results/coverage/{fastq}.csv", fastq=samples_fastq.ID),
        metadata=config['metadata']
    output:
        allcsv="results/coverage/allcoverage.csv",
        allxls="results/coverage/allcoverage.xlsx",
        multiqc="results/coverage/Coverage_fastq_info_mqc.txt"
    log:
       "log/coverage/allcoverage.log"
    conda:
        "../envs/rcran.yaml"
    script:
        "../scripts/combineCoverage_V2.R"


rule coverage_filtered:
    input:
      fw= rules.filter_kraken.output.o1,
      rv= rules.filter_kraken.output.o2,
      #fw="results/kraken2/{fastq}_R1_krakenFiltered.fastq.gz",
      #rv="results/kraken2/{fastq}_R2_krakenFiltered.fastq.gz",
      ref=config["refstrain"]
    output:
      out="results_filtered/coverage/{fastq}.csv"
    log:
       "log/coverage/filtered/{fastq}.log"
    shell:
       "sh scripts/fastq_info_ma_V2.sh  {input.fw} {input.rv} {input.ref} > {output.out} 2>{log}"

"""
Collects coverage result from ech sample and combines in one file
"""
rule collect_coverage_filtered:
    input:
        localres=expand("results_filtered/coverage/{fastq}.csv", fastq=samples_fastq.ID),
        metadata=config['metadata']
    output:
        allcsv="results_filtered/coverage/allcoverage.csv",
        allxls="results_filtered/coverage/allcoverage.xlsx",
        multiqc="results_filtered/coverage/Coverage_fastq_info_mqc.txt"
    log:
       "log/coverage/filtered/allcoverage.log"
    conda:
        "../envs/rcran.yaml"
    script:
        "../scripts/combineCoverage_V2.R"
        
        