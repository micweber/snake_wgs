"""
Panaroo: pangenome investigation
"""


rule panaroo:
    input:
        gff = expand("results_filtered/prokka/{fastq.ID}/{fastq.ID}.gff", fastq=samples_fastq.itertuples())
    output:
        directory("results_filtered/panaroo/")
    params:
        outdir = "results_filtered/panaroo/"
    benchmark:
        "log/benchmark/panaroo/.txt"
    log:
        "log/panaroo.log"
    conda:
        "../envs/panaroo2.yaml"
    shell:
        "mkdir -p {params.outdir} && panaroo -i {input} -o {params.outdir} --clean-mode strict"

