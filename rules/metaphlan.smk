

"""
Determine plasmid sequences using blast and PLSDB

"""

# Input are the filtered assemblies from spades assembly


rule metaphlan_mapping:
  input:
    fw="results/input_trimmed/{sample}/{sample}_R1_val_1.fq.gz",
    rv="results/input_trimmed/{sample}/{sample}_R2_val_2.fq.gz"
  output:
    profile="results/metaphlan/profiles/{sample}_profiled.txt",
    bowtie="results/metaphlan/bowtie/{sample}.bowtie2.bz2",
    sam="results/metaphlan/samfiles/{sample}.sam"
  params:
    db=config["metaphlandb"]
  log: "logs/metaphlan/metaphlan_{sample}.txt"
  benchmark:
    "benchmark/metaphlan/metaphlan_{sample}.txt"
  conda:
    "../envs/metaphlan.yaml"
  threads: 16
  resources: mem_mb=32000, cpus=8  
  priority: 4
  group: "metaphlan"
  shell:
    """
    metaphlan {input.fw},{input.rv} -t rel_ab_w_read_stats --bowtie2out {output.bowtie} --bowtie2db {params.db}  \
        --nproc {threads} --input_type fastq -s {output.sam} -o {output.profile} 2> {log}
    """
