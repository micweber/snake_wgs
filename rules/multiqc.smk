"""
MultiQC for FastQC
"""


"""
Helper: prepares a folders holding all results from fastqc
needed to link all files using expand otherwise snakemake will fail
"""

# Input are all (own,given) assemblies
#  directories for all results
rule prepare_folder:
    input:
        fastqc=expand("results/fastqc/{fastq}", fastq=samples_fastq.ID),
        quast=expand("results/quast/{sample}", sample=samples.ID)
    output:
        tmp_fastqc=(directory("tmp/fastqc")),
        tmp_quast=(directory("tmp/quast"))
    benchmark:
        "log/benchmark/prepare_folder_multiqc.txt"
    log:
        "log/multiqc/prepare_folder_multiqc.log"
    run:
        ## Create the directories
        os.makedirs("tmp/fastqc",exist_ok=True)
        os.makedirs("tmp/quast",exist_ok=True)
        for fastqc in input.fastqc:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(fastqc)
            shell("ln -s {abspath} {output.tmp_fastqc} >{log}")
        for quast in input.quast:
            # Better to symlink than to copy to save some space
            abspath = os.path.abspath(quast)
            shell("ln -s {abspath} {output.tmp_quast} >{log}")



"""
MultiQC for FastQC
"""
rule multiqc_fastqc:
    input:
        "tmp/fastqc",#takes tmp folder as input
    output:
        directory("results/multiqc_fastqc")
    threads:
        20
    conda:
        "../envs/multiqc.yaml"
    log:
        "log/multiqc_fastqc/multiqc_fastqc.log"
    benchmark:
        "log/benchmark/multiqc_fastqc/multiqc_fastqc.txt"
    shell:
        "multiqc --filename fastqc_assembly_report --outdir {output} {input}"



"""
MultiQC for non fastqc
"""
rule multiqc:
    input:
        quast="tmp/quast",#takes tmp folder as input
#        kraken_species=kraken_reads+"kraken_reads_per_species_mqc.txt",#kraken results for reads and species
#        kraken_genus=kraken_reads+"kraken_reads_per_genera_mqc.txt",#kraken results for reads and genus
#        kraken_species_assemblies=kraken_contigs+"kraken_contigs_per_species_mqc.txt",#kraken results for contigs and species
        coverage="results/coverage/Coverage_fastq_info_mqc.txt",#coverage
#        cansnper=cansnper_res+"CanSNPer_Subclades_mqc.txt" if re.search(config["genus"] , "Francisella", re.IGNORECASE) else "master.Snakefile", #uses cansper only if Francisella #data otherwise uses script as dummy input
        abricate_plasmidfinder="results/abricate/Abricate_Plasmidfinder_mqc.txt",
        abricate_vfdb="results/abricate/Abricate_vfdb_mqc.txt",
        abricate_resfinder="results/abricate/Abricate_Resfinder_mqc.txt",#output MQC format
        abricate_card="results/abricate/abricate_card_mqc.txt",#output MQC format
        abricate_ncbi="results/abricate/Abricate_ncbi_mqc.txt",#output MQC format
	amrfinder="results/amrfinderplus/AMRfinder_mqc.txt",
#        sistr=sistr_res+"SISTR_results_mqc.txt" if re.search(config["genus"] , "Salmonella", re.IGNORECASE) else "master.Snakefile",#uses cansper only if Salmonella data otherwise #uses script as dummy input
    output:
        directory("results/multiqc")
    conda:
        "../envs/multiqc.yaml"
    log:
        "log/multiqc/multiqc.log"
    benchmark:
        "log/benchmark/multiqc/multiqc.txt"
    shell:
        "multiqc --filename multiqc_report --outdir {output} {input.quast} {input.coverage} {input.abricate_plasmidfinder} {input.amrfinder} {input.abricate_resfinder} {input.abricate_card} {input.abricate_ncbi} {input.abricate_vfdb}"


rule multiqc_filtered:
    input:
        quast="tmp/quast/filtered",#takes tmp folder as input
#        kraken_species=kraken_reads+"kraken_reads_per_species_mqc.txt",#kraken results for reads and species
#        kraken_genus=kraken_reads+"kraken_reads_per_genera_mqc.txt",#kraken results for reads and genus
#        kraken_species_assemblies=kraken_contigs+"kraken_contigs_per_species_mqc.txt",#kraken results for contigs and species
        coverage="results_filtered/coverage/Coverage_fastq_info_mqc.txt",#coverage
#        cansnper=cansnper_res+"CanSNPer_Subclades_mqc.txt" if re.search(config["genus"] , "Francisella", re.IGNORECASE) else "master.Snakefile", #uses cansper only if Francisella #data otherwise uses script as dummy input
      #  abricate_plasmidfinder="results/abricate/Abricate_Plasmidfinder_mqc.txt",
       # abricate_vfdb="results/abricate/Abricate_vfdb_mqc.txt",
    #    abricate_resfinder="results/abricate/Abricate_Resfinder_mqc.txt",#output MQC format
     #   abricate_card="results/abricate/abricate_card_mqc.txt",#output MQC format
      #  abricate_ncbi="results/abricate/Abricate_ncbi_mqc.txt",#output MQC format
	#amrfinder="results/amrfinderplus/AMRfinder_mqc.txt",
#        sistr=sistr_res+"SISTR_results_mqc.txt" if re.search(config["genus"] , "Salmonella", re.IGNORECASE) else "master.Snakefile",#uses cansper only if Salmonella data otherwise #uses script as dummy input
    output:
        directory("results_filtered/multiqc")
    conda:
        "../envs/multiqc.yaml"
    log:
        "log/multiqc/filtered/multiqc.log"
    benchmark:
        "log/benchmark/multiqc/filtered/multiqc.txt"
    shell:
        "multiqc --filename multiqc_report --outdir {output} {input.quast} {input.coverage}"
        
        # {input.abricate_plasmidfinder} {input.amrfinder} {input.abricate_resfinder} {input.abricate_card} {input.abricate_ncbi} {input.abricate_vfdb}"
