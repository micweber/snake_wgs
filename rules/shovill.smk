"""
Assembly of the reads using shovill.
"""
#input: receive all forward and reverse fastq files
#output: is directoy for each sample

#18-00244-1_R1_val_1.fq.gz
#input/{fastq}_R1.fastq.gz

## Trimmed
## input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz

## Shovill can be assigned genome_size
# --gsize 1098626 \
rule shovill:
    input:
        fw="results/input_trimmed/{fastq}/{fastq}_R1_val_1.fq.gz",
        rv="results/input_trimmed/{fastq}/{fastq}_R2_val_2.fq.gz"
    output:
        directory("results/shovill/{fastq}")
    priority: 3
    threads: 8
    log:
        "log/shovill/{fastq}.log"
    conda:
        "../envs/shovill.yaml"
    group: "assembly"
    shell:
        "shovill --cpus {threads} \
            --R1 {input.fw} \
            --R2 {input.rv}\
            --outdir {output}\
            --tmpdir ./tmp \
            --force \
            2> {log}"


rule shovill_filtered:
    input:
        fw= rules.filter_kraken.output.o1,
        rv= rules.filter_kraken.output.o2
        #fw="results_filtered/{fastq}/{fastq}_R1_krakenFiltered.fastq.gz",
        #rv="results_filtered/{fastq}/{fastq}_R2_krakenFiltered.fastq.gz"
    output:
        directory("results_filtered/shovill/{fastq}")
    priority: 3
    threads: 8
    log:
        "log/shovill/filtered/{fastq}.log"
    conda:
        "../envs/shovill.yaml"
    group: "assembly"
    shell:
        "shovill --cpus {threads} \
            --R1 {input.fw} \
            --R2 {input.rv}\
            --outdir {output}\
            --tmpdir ./tmp \
            --force \
            2> {log}"


"""
Renames contigs to have short contig names (required by some tools)
"""
#input is result from shovill


rule shovill_rename_contigs:
    input:
        "results/shovill/{sample}",
    output:
        "results/finalAssembly/{sample}.fasta"
    priority: 3
    log:
        "log/rename/{sample}.log"
    benchmark:
        "log/rename/{sample}_benchmark.txt"
    shell:
        "sed -r 's/>([^ ]+).*/>{wildcards.sample}_\\1/g' {input}/contigs.fa > {output} 2> {log}"
        
        
rule shovill_rename_contigs_filtered:
    input:
        "results_filtered/shovill/{sample}",
    output:
        "results_filtered/finalAssembly/{sample}.fasta"
    priority: 3
    log:
        "log/rename/filtered/{sample}.log"
    benchmark:
        "log/rename/filtered/{sample}_benchmark.txt"
    shell:
        "sed -r 's/(>[^ ]+) .*/\\1/g' {input}/contigs.fa > {output} 2> {log}"
