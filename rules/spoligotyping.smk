"""
Spoligotyping using raw reads 
"""

rule spoligotyping:
    input:
        fw="results/input_trimmed/{sample}/{sample}_R1_val_1.fq.gz",
        rv="results/input_trimmed/{sample}/{sample}_R2_val_2.fq.gz"
    output:
        "results/spoligotyping/{sample}.spoligo.txt"
    params:
        path="results/spoligotyping",
        outdir="results/tmp/spoligotyping/{sample}"
    log:
        "log/spoligotyping/{sample}.log"
    conda: 
        "../envs/spotyping.yaml"
    shell:
        """
        mkdir -p {params.outdir}
        SpoTyping.py {input.fw} {input.rv} -O {params.outdir} --noQuery &> {log}
        mv {params.outdir}/SpoTyping {output}
        rm -r {params.outdir}
        """


rule combine_spoligotyping:
    input:
        spofiles=expand("results/spoligotyping/{sample}.spoligo.txt", sample=samples.ID)
    output:
        allcsv="results/spoligotyping/all_spoligotyping.csv",
        allxls="results/spoligotyping/all_spoligotyping.xlsx",
        multiqc="results/spoligotyping/Spoligotyping_mqc.txt"
    log:
        "log/spoligotyping/combine_spoligotyping.log"
    benchmark:
        "log/benchmark/spoligotyping/combine_spoligotyping.txt"
    conda:
        "../envs/rcran.yaml"
    script:
        "../scripts/combine_spoligotyping.R"
