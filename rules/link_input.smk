import pandas as pd

## Reference:
## https://git-r3lab.uni.lu/aurelien.ginolhac/snakemake-workflows/-/tree/master/workflow

def get_fastq(wildcards):
    return samples_fastq.loc[(wildcards.fastq), ["File_left_reads","File_right_reads"]].dropna()
    
#def get_fastq_filtered(wildcards):
#    return expand("results/kraken2/filtered/{wildcards.fastq}_{group}_krakenFiltered.fastq.gz",
#                          group=[1, 2], **wildcards)

    
def get_fasta(wildcards):
    return samples_fasta.loc[(wildcards.sample), ["File_assembly"]].dropna()


rule link_samples_fastq:
  input:
    get_fastq
  output:
    fw="input/{fastq}_R1.fastq.gz",
    rv="input/{fastq}_R2.fastq.gz"
  shell:
    """
    ln -s {input[0]} {output.fw} 
    ln -s {input[1]} {output.rv}
    """


#rule link_samples_fastq_filtered:
#  input:
#    get_fastq_filtered
#  output:
#    fw="results/kraken2/filtered/{fastq}_R1_krakenFiltered.fastq.gz",
#    rv="results/kraken2/filtered/{fastq}_R2_krakenFiltered.fastq.gz"
#  shell:
#    """
#    ln -s {input[0]} {output.fw} 
#    ln -s {input[1]} {output.rv}
#    
#    """



# Input: list of already present assembled fastas

#rule link_samples_fasta:
#  input:
#    get_fasta
#  output:
#    "results/finalAssembly/{sample}.fasta",
#  shell:
#    """
#    ln -s {input[0]} {output} 
#    """    
    