
"""
Bakta for gene prediction and annotation

"""
rule bakta:
    input:
        "results/finalAssembly/{sample}.fasta"
    output:
        gff="results/bakta/{sample}/{sample}.gff3",
        faa="results/bakta/{sample}/{sample}.faa",
        fna="results/bakta/{sample}/{sample}.fna",
        tsv="results/bakta/{sample}/{sample}.tsv",
        txt="results/bakta/{sample}/{sample}.txt"
    benchmark:
        "log/benchmark/bakta/{sample}.txt"
    log:
        "log/bakta/{sample}.txt"
    conda:
        "../envs/bakta.yaml"
    params:
        baktadb=config.get("baktadb",""),
        gcode=config.get("genetic_code",11),
        outdir="results/tmp/bakta/{sample}"
    threads: 8
    priority: 3    
    shell:
        """
        bakta --db {params.baktadb} --verbose --output {params.outdir}  \
            --prefix {wildcards.sample} --skip-trna --skip-tmrna --skip-rrna \
            --skip-ncrna --skip-ncrna-region --skip-pseudo --skip-sorf --skip-gap \
            --force --threads {threads} {input} &> {log}
        cp --verbose {params.outdir}/{wildcards.sample}.gff3 {output.gff} 
        cp --verbose {params.outdir}/{wildcards.sample}.faa {output.faa}
        cp --verbose {params.outdir}/{wildcards.sample}.fna {output.fna}
        cp --verbose {params.outdir}/{wildcards.sample}.tsv {output.tsv}
        cp --verbose {params.outdir}/{wildcards.sample}.txt {output.txt}
        rm --verbose -rf results/tmp/bakta/{wildcards.sample} >> {log}
        """
