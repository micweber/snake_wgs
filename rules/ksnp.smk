"""
get ksnp
#downloads and extracts ksnp
"""
rule get_ksnp:
    input:
        "config.yaml"#dumper input
    output:
        folder=directory("ksnp"),
        file=temp("ksnp.zip")
    benchmark:
        "log/benchmark/get_ksnp.txt"
    log:
        "log/ksnp/get_ksnp.log"
    shell:
        "wget https://sourceforge.net/projects/ksnp/files/kSNP3.1_Linux_package.zip -O {output.file} &> {log} &&\
         unzip {output.file} -d {output.folder} &>>{log}&&\
         tail -n +8 ksnp/kSNP3.1_Linux_package/kSNP3/kSNP3 > ksnptmp &&\
         chmod +x ksnptmp &&\
         sed -i \"1iset kSNP=$(pwd)/ksnp/kSNP3.1_Linux_package/kSNP3\" ksnptmp &&\
         sed -i '1i#v3.92' ksnptmp &&\
         sed -i '1i#!/bin/tcsh' ksnptmp &&\
         mv ksnptmp ksnp/kSNP3.1_Linux_package/kSNP3/kSNP3"
"""
prepares ksnp
creates input table
creates multiple fasta
runskchooser
"""

## knsprep/"#folder holding files to prepare ksnp (input_ksnp,cp.fasta.kchooser.report,...)
#create table, create multifasta, run kchoser to select k

rule prepare_ksnp:
    input:
        expand("results/finalAssembly/{sample}.fasta",sample=samples.ID) # list of all assembled files
    output:
        kSNP3infile=("tmp/ksnp_prep/input_ksnp"),#first step: creates input table
        fasta=("tmp/ksnp_prep/cp.fasta"),#second step: creates multiple fasta
        kchooser=("tmp/ksnp_prep/Kchooser.report")#third step: runkchooser
    params:
        asdir="results/finalAssembly", # directory of all assemblies
        ksnpdir="ksnp" #path to ksnp ksnp binaries
    benchmark:
        "log/benchmark/prepare_ksnp.txt"
    log:
        "log/ksnp/prepare_ksnp.log"
    shell:
        "{params.ksnpdir}/kSNP3.1_Linux_package/kSNP3/MakeKSNP3infile {params.asdir} {output.kSNP3infile} A &>{log}&&\
         printf 'C' | {params.ksnpdir}/kSNP3.1_Linux_package/kSNP3/MakeFasta  {output.kSNP3infile} {output.fasta} &>>{log}&&\
        {params.ksnpdir}/kSNP3.1_Linux_package/kSNP3/Kchooser  {output.fasta} 1>{output.kchooser} 2>>{log}"

"""
run ksnp
"""
rule run_ksnp:
    input:
        kchooser="tmp/ksnp_prep/Kchooser.report",#results from runkchooser
        kSNP3infile="tmp/ksnp_prep/input_ksnp"#ksnpinput table
    output:#output is folder ksnp
        directory("results/ksnp"),
    params:
        kvalue="19",
        asdir="results/finalAssembly", # directory of all assemblies
        ksnpdir="ksnp" #path to ksnp ksnp binaries
    benchmark:
        "log/benchmark/ksnp/runksnp.txt"
    log:
        "log/ksnp/run_ksnp.log"

    threads: 16
    shell:
       " {params.ksnpdir}/kSNP3.1_Linux_package/kSNP3/kSNP3 -k {params.kvalue} -ML  -NJ -core  -vcf -in {input.kSNP3infile}  -CPU {threads}  -outdir {output} 2>{log}"



"""
helper
copies ksnp trees to a tmp directory as drawing them needs a file as input not a  directoy
"""
rule cpksnp:
    input:
        "results/ksnp"
    output:
        pars=temp("tmp/tree.parsimony.tre"),#file with parsimoney tree
        ML=temp("tmp/tree.ML.tre"),#file with ML tree
        NJ=temp("tmp/tree.NJ.tre")#file with NJ tree
    shell:
        "cp {input}/tree.parsimony.tre  {output.pars}&&\
         cp {input}/tree.ML.tre {output.ML}&& \
         cp {input}/tree.NJ.tre {output.NJ}"


"""
Draws parsimoney tree from ksnp results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed sample
"""
rule drawkSNPparsimoneytree:
    input:
        metadata=config['metadata'],
        #ALways use "newick" as variabe name
        newick="tmp/tree.parsimony.tre",#file with parsimoney tree
    output:
        #ALways use "pdf" as variabe name
        pdf="results/trees/kSNP_parsimoney_tree.pdf",
        newick="results/trees/kSNP_parsimoney_tree.nw"#tree in newick format with exchanded IDs into common names
    conda:
        "../envs/rcran.yaml"
    params:
        #alsways use params as variable name
        title="Parsimoney tree from ksnp3"
    log:
        "log/ksnp/drawkSNPparsimoneytree.log"
    benchmark:
        "log/benchmark/ksnp/drawkSNPparsimoneytree.txt"
    script:
        "../scripts/plottree.R"

"""
Draws Maximum Likelihood  tree from ksnp results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed sample
"""
rule drawkSNPMLtree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #ALways use "newick" as variabe name
        newick="tmp/tree.ML.tre",#file with ML tree
    output:
        #ALways use "pdf" as variabe name
        pdf="results/trees/kSNP_ML_tree.pdf",
        newick="results/trees/kSNP_ML_tree.nw"#tree in newick format with exchanded IDs into common names
    params:
        #alsways use params as variable name
        title="Maximum Likelihood tree from ksnp3"
    conda:
        "../envs/rcran.yaml"
    log:
        "log/ksnp/drawkSNPMLtree.log"
    benchmark:
        "log/benchmark/ksnp/drawkSNPML.txt"
    script:
        "../scripts/plottree.R"

"""
Draws Nieghbour Joining  tree from ksnp results
Uses metadata of KNOWN samples (typestrains etc) combined with newly analzed sample
"""
rule drawkSNPNJtree:
    input:
        metadata=config['metadata'],#metdada of known reference samples (typestrains,...)
        #ALways use "newick" as variabe name
        newick="tmp/tree.NJ.tre",#file with NJ tree
    output:
        #ALways use "pdf" as variabe name
        pdf="results/trees/kSNP_NJ_tree.pdf",
        newick="results/trees/kSNP_NJ_tree.nw"#tree in newick format with exchanded IDs into common names
    params:
        #always use params as variable name
        title="Neighbour Joining tree from ksnp3"
    conda:
        "../envs/rcran.yaml"
    log:
        "log/ksnp/drawkSNPNJtree.log"
    benchmark:
        "log/benchmark/ksnp/drawkSNPNJ.txt"
    script:
        "../scripts/plottree.R"
