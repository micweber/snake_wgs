"""
Assembly of plasmids using spades
"""
# receive all forward and reverse fastq files
# output is directoy for each sample
rule spades_plasmid:
    input:
        fw= rules.filter_kraken.output.o1,
        rv= rules.filter_kraken.output.o2
    output:
        directory("results_filtered/spades_plasmid/{fastq}")
    threads:
        16
    benchmark:
        "benchmark/plasmidspades/{fastq}.txt"
    log:
        "log/plasmidspades/{fastq}.txt"
    conda:
      "../envs/spades.yaml"
    shell:
        """
        plasmidspades.py -t {threads} \
            --pe1-1 {input.fw} \
            --pe1-2 {input.rv} \
            --careful \
            -o {output}
            
        """   
        
  