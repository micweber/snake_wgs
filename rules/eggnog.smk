


rule emapper:
    input:
        "results/bakta/{sample}/{sample}.faa"
    output: 
        "results/eggnog/{sample}.emapper.hits", 
        "results/eggnog/{sample}.emapper.annotations"
    log: 
        "log/eggnog/{sample}.eggnog.log"
    conda:
        "envs/emapper.yaml"
    params:
        outdir="results/eggnog/{sample}"
    threads:  8
    shell: 
        """
            emapper.py -i {input} --itype proteins -m diamond --sensmode default \
             --evalue 0.001 --go_evidence all --output {wildcards.sample} --output_dir {params.outdir} \
             --tax_scope bacteria --override --cpu {threads} 2> {log}
        """