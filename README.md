# snake_wgs

Simple and tailored workflows and databases for WGS analysis of Chlamydia, Mycobacteria and Mycoplasma isolates.
Based on WGSBAC (https://gitlab.com/FLI_Bioinfo/WGSBAC)

## Installation
You need a current Snakemake version and Python to run the Pipeline.

### Bioconda and Mamba
We recommend the installation with [mamba](https://github.com/mamba-org/mamba) and the [Bioconda channel](https://bioconda.github.io/)
```{bash}
mamba install -c bioconda snakemake
```

### Generate sample tables
Prior to pipeline execution, fastq and fasta files need to be summarised within a samples table.
Run the Python script to identify paired-end fastq files. Sample IDs are automatically estimated from file names. Parameters `--sep [Default _]` and `--pos` can be specified to manually extract the sample ID.
The script also allows to merge files if multiple paired-end files for the same ID are detected.

```{bash}
./scripts/init/wgs_init_samples.py <FASTQ-Directory> 

```

### Set up the configuration file for Snakemake
The following items are mandatory in the config file: samples, refstrain and genetic_code.
By default genetic code in Bacteria is 11.

### Run the snakemake Pipeline

Start a dry run to get an impression about the performed jobs.
```{bash}
snakemake --use-conda --cores 4 -k -p -n
```

Start the snakemake run.
```{bash}
snakemake --use-conda --cores 4 -k -p
```

Start the snakemake on the HPC.
```{bash}
snakemake --profiles slurmwgs
```

