#!/usr/bin/env python

import re
import glob
from pathlib import Path
import os
import numpy as np
import pandas as pd
import argparse
import subprocess

## Merge FASTQ files if mulitple fastq for the same ID exist
#from preprocessFunctions import *


def writeLogFile(line,file,mode):
    with open(file,mode) as f:
        f.write(line+"\n")

def wgs_init_mergeFastq(fastqTable,inputDir):
    #
    # filePaths = sorted(filter(os.path.isfile,glob.glob(inputDir+"/**/*",recursive=True)))
    #
    # # Filter files fastq,fq,gz files
    # pat = re.compile('.fastq.gz$|.fastq$|.fq.gz$|.fq$')
    # fastqPaths = [ s for s in filePaths if re.search(pat,s) ]
    #
    # # Transform relative paths to absolute paths
    # absFastqPaths = [os.path.abspath(f) for f in fastqPaths]
    #
    # ## Generate the fastq table
    # fastqTable = build_sample_fastq_table(absFastqPaths,split_sep = "_",split_pos = 2)

    ## Get the duplicated IDs
    ids = fastqTable["ID"]

    ## Find the duplicated IDs in the data frame
    duplicatedIDs = ids[ids.duplicated()].unique()

    # Create merge directory
    mergeDir = os.path.join(os.path.abspath(inputDir), "merged")
    os.makedirs(mergeDir,exist_ok=True)

    ## Create unmerged directory to store the original files
    unmergedDir = os.path.join(os.path.abspath(inputDir), "unmerged")
    os.makedirs(unmergedDir,exist_ok=True)

    logFile = os.path.join(mergeDir,"merged.log")

    if len(duplicatedIDs) >= 1:
        for id in duplicatedIDs:
            subTable = fastqTable.query("ID == @id")
            fwfiles = ' '.join(subTable["File_left_reads"])
            rvfiles = ' '.join(subTable["File_right_reads"])
            call = ''.join(["cat ",fwfiles," > ",mergeDir,"/",id,"_merged_1.fq.gz"])
            #print(call)
            ## Write call to log file
            writeLogFile(call,logFile,'w')
            ret = subprocess.run(call,shell=True)
            call = ''.join(["cat ",rvfiles," > ",mergeDir,"/",id,"_merged_2.fq.gz"])
            ## Write call to log file
            writeLogFile(call,logFile,'a')
            #print(call)
            ret = subprocess.run(call,shell=True)

            ## Move the original files to folder unmerged
            for f in subTable["File_left_reads"]:
                os.rename(f,os.path.join(unmergedDir,os.path.basename(f)))

            for f in subTable["File_right_reads"]:
                os.rename(f,os.path.join(unmergedDir,os.path.basename(f)))
