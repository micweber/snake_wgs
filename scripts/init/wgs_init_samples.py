#!/usr/bin/env python

import re
import glob
from pathlib import Path
import os
import numpy as np
import pandas as pd
import argparse
import subprocess
import sys
from mergeInputFiles import *

def most_common(lst):
    return max(set(lst), key=lst.count)

## Function build_sample_fasta_table

def build_sample_fasta_table(fastaPaths,split_sep = None,split_pos = None):

    # Generate FASTA table
    if len(fastaPaths) > 0:
    # Reset the split sep and pos
        if split_sep is None:
            split_sep = '_|.fasta$|.fna$'

        if split_pos is None:
            # Get the file base names without  extension
            stemFiles = [re.sub(".fasta$|.fna$","",os.path.basename(elem)) for elem in fastaPaths]
            ## Guess the ID part
            splits = [list((re.split(split_sep, x))) for x in stemFiles]

            itemFreq = []
            for i, parts in enumerate(zip(*splits)):
                items = set(parts)
                itemFreq.append(len(items))

            # index_max provides the position in the split for the ID
            split_pos = np.argmax(itemFreq[:2])

        # Transform relative paths into abspaths
        absFastaPaths = [os.path.abspath(f) for f in fastaPaths]

        idVector = [list((re.split(split_sep, os.path.basename(f))))[split_pos] for f in fastaPaths]
        # Generate FASTA table
        data = {'ID': idVector,
                'Common.name': idVector,
                'File_left_reads': "",
                'File_right_reads': "",
                'File_assembly': absFastaPaths
                }
        fastaDF = pd.DataFrame(data)
        return fastaDF

## Function build_sample_fastq_table

def build_sample_fastq_table(fastqPaths,split_sep = None,split_pos = None):

    ## Avoid merged and unmerged directories

    ## Check if all files are paired end
    if len(fastqPaths) % 2 != 0:
        print("Not all files are paired end")
        sys.exit()

    if split_sep is None:
        split_sep = '_|.fasta$|.fna$'

    # Extract the basenames
    baseFiles = [os.path.basename(f) for f in fastqPaths]
    stemFiles = [re.sub(".fastq.gz$|.fastq$|.fq.gz$|.fq$","",elem) for elem in baseFiles]

    ## Check if merged files
    mergedFlag = 1 if "merged" in stemFiles[0] else 0

    ## Guess the ID part
    splits = [list((re.split(split_sep, x))) for x in stemFiles]

    ## Find out the most common number of splits
    ## Because different files might have different compostion of library names and # ID
    ## For Example with and without library name
    ##  e.g. NA132_ID1234_1.fastq, NA132_ID1234_lib323_1.fastq
    lenLists = [len(x) for x in splits]
    idx = [i for i,ll in enumerate(lenLists) if ll == most_common(lenLists)]
    # Special case splits 0

    # Special case splits 1
    # Define the pos in the split list
    if split_pos is None:
        itemFreq = []
        for i, parts in enumerate(zip(*splits)):
            items = set(parts)
            itemFreq.append(len(items))

        # split_pos provides the position in the split for the ID
        if(len(baseFiles) == 2):
            split_pos = 0
        elif mergedFlag:
            split_pos = 0
        else:
            split_pos = np.argmax(itemFreq)


    # Define index for forward and reverse paired end files
    left_index = [i for i in range(0,len(fastqPaths)) if i % 2 == 0]
    right_index = [i for i in range(0,len(fastqPaths)) if i % 2 == 1]

    # Transform relative paths to absolute paths
    absFastqPaths = [os.path.abspath(f) for f in fastqPaths]

    # Define forward and reverse vector
    fwVector = [absFastqPaths[x] for x in left_index]
    revVector = [absFastqPaths[x] for x in right_index]
    # Define the idVector for fastq table
    idVector = [list((re.split(split_sep, os.path.basename(f))))[split_pos] for f in fwVector]

    # Define FASTQ meta data data.frame
    data = {'ID': idVector,
            'Common.name': idVector,
            'File_left_reads': fwVector,
            'File_right_reads': revVector,
            'File_assembly': ""
            }
    fastqDF = pd.DataFrame(data)
    return fastqDF


parser = argparse.ArgumentParser("Generate genomes meta data table from input directory")
parser.add_argument("input", help="The directory which will recursively be screend for fastqfiles", type=str,default=os.getcwd())

## Add optional arguments split separator and numbers
parser.add_argument("--sep", help="Split the file name by this separator to get the sample ID", type=str)
parser.add_argument("--pos", help="Split Position of the splitted file to extract the sample ID", type=int)
parser.add_argument("--omit", help="Omit pattern for subdirectories to excluded ", type=str)
parser.add_argument("--output", help="Output filename of the metadata table", type=str)

# Parse the arguments
args = parser.parse_args()

# Make sure that --sep and --pos argument are always called together
## sep and pos have default values
#if bool(args.sep != None) != bool(args.pos != None):
#    parser.error('Argument sep and pos must be specified together ')

# Define the split string to locate the ID in the filename

## Define the input directory
inputDir = args.input
## Check if the input dir exists
if not os.path.exists(inputDir):
    sys.exit("ERROR : Input directory does not exist")

print("input directory is : " + inputDir)

# Default is underscore
split_str = "_"
if args.sep is not None:
    split_str = args.sep

split_pos = None
if args.pos is not None:
    split_pos = args.pos


# Get sorted file paths, check wether all files
filePaths = sorted(filter(os.path.isfile,glob.glob(inputDir+"/**/*",recursive=True)))

## Check if all fastq files are gzipped
## For later merging step we don consider unzipped fastq files
pat = re.compile('.fastq$|.fq$')
unzippedFiles = [ s for s in filePaths if re.search(pat,s) ]
if len(unzippedFiles) != 0:
    print("\nERROR: Please make sure that all fastq files are gz-compressed")
    sys.exit("\n")


# Filter files fastq,fq,gz files
pat = re.compile('.fastq.gz$|.fastq$|.fq.gz$|.fq$')
fastqPaths = [ s for s in filePaths if re.search(pat,s) ]

## Delete fastq files in merged and unmerged directory
firstPaths = [ s for s in fastqPaths if not re.search('merged',s) ]

# If there are at least one pair of FASTQ files
if len(firstPaths) > 1:
    fastqDF = build_sample_fastq_table(firstPaths,split_str,split_pos)
    ## Print the top 5 IDs
    print("Extracted FASTQ IDs:")
    print(fastqDF.loc[:,"ID"])
    print("\n")
    ## Check the number of rows & Check the unique number of IDs
    rowNumber = fastqDF.shape[0]
    idNumber = len(fastqDF.ID.unique())
    print("Number of paired end FASTQ files in the directory : " + str(rowNumber))
    print("Number of unique IDs : " + str(idNumber))

    if((rowNumber - idNumber) > 0):
        ids = fastqDF["ID"]
        ## Find the duplicated IDs in the data frame
        duplicatedIDs = ids[ids.duplicated()].unique()
        print("Number of IDs with duplicated files : " + str(len(duplicatedIDs)) + "\n")
        print("Duplicated IDs: ")
        print(duplicatedIDs)
        print("WARNING: Some IDs are duplicated, consider merging of fastq files. "+"\n")
        res = input("Merge FASTQ files with duplicated IDs ?  ([y] / n) :") or "y"
        if(res == "y"):
            ## Merge the files
            print("Merge Fastqs")
            wgs_init_mergeFastq(fastqDF,inputDir)
            ## Build the table for the merged files
            ## Update the list of file filePaths
            filePaths = sorted(filter(os.path.isfile,glob.glob(inputDir+"/**/*",recursive=True)))

            # Filter files fastq,fq,gz files
            pat = re.compile('.fastq.gz$|.fq.gz$')
            fastqPaths = [ s for s in filePaths if re.search(pat,s) ]

            ## Delete fastq files in merged and unmerged directory
            mergedPaths = [ s for s in fastqPaths if re.search('_merged_',s) ]
            mergedFastqDF = build_sample_fastq_table(mergedPaths,"_",0)

            ## Rebuild the original table without merged folder
            firstPaths = [ s for s in fastqPaths if not re.search('merged',s) ]

            if(len(firstPaths) > 1):
                originalFastqDF = build_sample_fastq_table(firstPaths,split_str,split_pos)
            else:
                originalFastqDF = pd.DataFrame()

            ## Combine merged duplicated fastq and unique fastq files
            fastqDF = pd.concat([originalFastqDF,mergedFastqDF])
            ## Sort the combined data.frame by ID
            fastqDF = fastqDF.sort_values("ID")

else:
    fastqDF = pd.DataFrame()

# Locate fasta files
pat = re.compile('.fasta$|.fna$')
fastaPaths = [ s for s in filePaths if re.search(pat,s) ]

if len(fastaPaths) >= 1:
    fastaDF = build_sample_fasta_table(fastaPaths)
    print("Extracted IDs of files :")
    print(fastaDF.loc[:,"ID"])
    print("\n")
    ## Check the number of rows & Check the unique number of IDs
    rowNumber = fastaDF.shape[0]
    idNumber = len(fastaDF.ID.unique())
    print("Number of FASTA files in the directory : " + str(rowNumber))
    print("Number of unique IDs : " + str(idNumber))
    if((rowNumber - idNumber) > 0):
        print("WARNING: Some IDs are duplicated, please check the ID split parameters. "+"\n")
else:
    fastaDF = pd.DataFrame()

# concatenate fastq and fasta table
finalDF = pd.concat([fastqDF,fastaDF])

# Save to output directory
if args.output:
    outputFile = args.output
else:
    outputFile = "sample_meta_table.csv"

# Write FASTQ file
finalDF.to_csv(outputFile,index=False,sep="\t")
print(outputFile + " printed" + "\n")
