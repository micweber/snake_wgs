require(writexl)

quastfolders <- snakemake@input[["localres"]]
#quastfolders=paste("results",dir("results/", pattern="T"),"quast",sep="/")
cat("read folder list \n",file=snakemake@log[[1]],sep="")

## Read all quast results files

fileList=paste(quastfolders,"report.tsv",sep="/")
dataList <- lapply(fileList,read.delim,as.is=T,check.names=F)
cat("read local files \n",file=snakemake@log[[1]],sep="",append=T)


## Combine all data
allData <- Reduce(x=dataList,f="merge")

write.table(allData,snakemake@output[["allcsv"]],row.names=T,col.names=NA,sep="\t")
write_xlsx(allData,snakemake@output[["allxls"]])
cat("successfully finnished \n",file=snakemake@log[[1]],sep="",append=T)
